class DownloadError(Exception):
    """Exception to handle errors while downloading the object"""
    pass

class ProtocolNotSupportedError(Exception):
    """Exception to be raised when the protocol is not supported yet"""
    pass
