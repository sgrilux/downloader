#!/usr/bin/env python3

import sys
import threading

from loader import loader

class downloader(threading.Thread):
    def __init__(self, url):
        threading.Thread.__init__(self)
        self.url = url

    def run(self):
        """ Start the thread """
        try:
            loader_proto = loader(self.url)
            loader_proto.download()
        except Exception as e:
            print(e)

def usage():
    print(f"""
        Usage:
          ./{sys.argv[0]} <url-list>

        e.g.
            ./downloader.py s3://my-test-bucket/file-to-download http://de.releases.ubuntu.com/12.04/ubuntu-12.04.5-desktop-i386.iso
    """)
    sys.exit(1)

if __name__ == '__main__':

    if len(sys.argv) == 1:
        usage()

    urls = sys.argv[1:]
    downloader_threads = []

    # Creating a list of downloader threads for each url
    for url in urls:
        downloader_threads.append(downloader(url))

    # Starting all threads
    for downloader in downloader_threads:
        downloader.start()
