# Downloader

Multiprotocol downloader
The script makes use of Thread in order to be able to download files in parallel.

## Supported protocols

Currently the script supports only s3, http protocols.
The script gets the protocol to be used by the url.
For S3 you need to set up your aws credential in order to download from it.

## Support new protocol

In order to support a new protocol create a new module underneath the `protocols` folder
with a download function and import the file into `__init__.py` in the same folder.

## Usage

```bash
./downloader.py <url-list>
```

e.g.
```bash
./downloader.py s3://my-test-bucket/file-to-download http://de.releases.ubuntu.com/12.04/ubuntu-12.04.5-desktop-i386.iso
```

## Requirements

Install requirements. It's advisable to use `pipenv` or anyother virtual environment.

```bash
pipenv install
```

Otherwise install requirements just with pip

```bash
pip install -r requirements.txt
```

## Error handling

Two Exception have been created to manage exceptions:
-   ProtocolNotSupportedError
-   DownloadError

The first manage a generic error which will be thrown in case the protocol is not managed.
While the second is a Generic Exception to be thrown in case an error occurred during the download.

## Improvements

-   Unit Testing
