import boto3
import logging
import re

from exceptions import DownloadError
from botocore.exceptions import ClientError, NoCredentialsError
from . import get_logger

client = boto3.client('s3')

logger = get_logger(__name__)

def download(url):
    """ Download S3 Object """
    bucket_name = re.search(r'(s3://)([a-z].*)\/',url).group(2)
    file_name = url.rsplit('/', 1)[-1]

    download_file = f'./{file_name}'
    logger.info(f'Downloading {file_name}')

    try:
        client.download_file(bucket_name,
                             file_name,
                             download_file)
        logger.info(f'{file_name} downloaded succesfully!')
    except ClientError as err:
        raise DownloadError(f'[S3] Error occurred downloading file {url}')
    except NoCredentialsError as err:
        raise DownloadError(f'[S3] Please configure your aws client with your credentials to access the bucket')
