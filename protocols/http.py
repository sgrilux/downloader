import logging
import urllib.request

from urllib.error import HTTPError

from exceptions import DownloadError
from . import get_logger

logger = get_logger(__name__)

def download(url):
    """ HTTP Download """
    file_name = url.rsplit('/', 1)[-1]
    download_file = f'./{file_name}'
    logger.info(f'Downloading {file_name}')
    try:
        urllib.request.urlretrieve(url, download_file)
        logger.info(f'{file_name} downloaded succesfully!')
    except HTTPError as err:
        raise DownloadError(f'[HTTP] An error occurred downloading file {url}: {err}')
