import importlib.util
import os

from exceptions import ProtocolNotSupportedError

class loader(object):
    """Class used to handle different type of protocols"""

    def __init__(self, url):
        # Grabbing the protocol from the url
        self.module = url.split(':')[0].lower()
        self.url = url

        spec = importlib.util.find_spec('protocols.'+self.module)
        if spec:
            self.module = spec.loader.load_module()
        else:
            raise ProtocolNotSupportedError(f'Protocol {self.module} is not supported!')


    def download(self):
        self.module.download(self.url)
